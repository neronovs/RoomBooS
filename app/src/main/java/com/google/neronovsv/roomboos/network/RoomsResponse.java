package com.google.neronovsv.roomboos.network;

import com.google.neronovsv.roomboos.model.Room;

import java.util.ArrayList;
import java.util.List;

public class RoomsResponse {

    private List<Room> rooms;

    public List<Room> getRooms() {
        if (rooms == null) {
            rooms = new ArrayList<>();
        }
        return rooms;
    }

}

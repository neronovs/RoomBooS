package com.google.neronovsv.roomboos.network.usecase;

import com.google.neronovsv.roomboos.model.Room;

import java.util.List;

import rx.Observable;

public interface RoomsRepository {

    Observable<List<Room>> getRoomsObservable(String searchKey);

}

package com.google.neronovsv.roomboos.screens.details;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.widget.Adapter;
import android.widget.TextView;

import com.google.neronovsv.roomboos.model.Room;

import java.util.List;

public interface DetailView extends com.google.neronovsv.roomboos.screens.general.LoadingView {

    void fillWidgets(String detailed_tv_location, String detailed_tv_size_capacity,
                     String detailed_tv_name, String detailed_tv_equipment);

}



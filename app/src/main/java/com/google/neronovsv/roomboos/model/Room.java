package com.google.neronovsv.roomboos.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class Room
        extends RealmObject
        implements Serializable{

    public Room() {}

    public Room(List<String> avail, String size, List<String> images,
                String name, String location, List<String> equipment, int capacity) {
                this.avail = (RealmList<String>) avail;
                this.size = size;
                this.images = (RealmList<String>) images;
                this.name = name;
                this.location = location;
                this.equipment = (RealmList<String>) equipment;
                this.capacity = capacity;
    }

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("equipment")
    @Expose
    private RealmList<String> equipment = null;
    @SerializedName("size")
    @Expose
    private String size;
    @SerializedName("capacity")
    @Expose
    private Integer capacity;
    @SerializedName("avail")
    @Expose
    private RealmList<String> avail = null;
    @SerializedName("images")
    @Expose
    private RealmList<String> images = null;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<String> getEquipment() {
        return equipment;
    }

    public void setEquipment(List<String> equipment) {
        this.equipment = (RealmList<String>) equipment;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public List<String> getAvail() {
        return avail;
    }

    public void setAvail(List<String> avail) {
        this.avail = (RealmList<String>) avail;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = (RealmList<String>) images;
    }
}
package com.google.neronovsv.roomboos.screens.main;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.neronovsv.roomboos.R;
import com.google.neronovsv.roomboos.model.Room;
import com.google.neronovsv.roomboos.utils.CalendarTreatment;

import java.util.ArrayList;
import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    ArrayList<Room> getmRoomsList() {
        return mRoomsList;
    }

    private ArrayList<Room> mRoomsList;
    private ArrayList<Room> itemsCopy;

    private final OnItemClickListener mOnItemClickListener;
    private final View.OnClickListener mInternalListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Room room = (Room) view.getTag();
            mOnItemClickListener.onItemClick(view, room);
        }
    };

    interface OnItemClickListener {
        void onItemClick(@NonNull View view, @NonNull Room room);
    }

    DataAdapter(ArrayList<Room> rooms, OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
        mRoomsList = new ArrayList<>();
        mRoomsList.addAll(rooms);
        itemsCopy = new ArrayList<>();
        itemsCopy.addAll(rooms);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Room room = mRoomsList.get(position);

        holder.itemView.setOnClickListener(mInternalListener);
        holder.itemView.setTag(room);

        holder.tv_name.setText(room.getName());
        holder.tv_location.setText(room.getLocation());
        holder.tv_size_capacity.setText(String.valueOf(room.getCapacity()));
        StringBuilder equipmet = new StringBuilder();
        for (String el : room.getEquipment()) {
            equipmet.append(el).append("; ");
        }
        holder.tv_equipment.setText(equipmet.toString());

        fillTimeLine(position, holder, room);
    }

    private void fillTimeLine(int position, ViewHolder holder, Room room) {
        List<String> listAvail = room.getAvail();

        for (int j = 0; j < listAvail.size(); j++) {
            String timeFrom = listAvail.get(j).substring(0, 6);
            String timeTo = listAvail.get(j).substring(8);
            int timeRange = Integer.valueOf(timeTo.substring(0, 2)) -
                    Integer.valueOf(timeFrom.substring(0, 2));
            int timeFromIntMinus7 = Integer.valueOf(timeFrom.substring(0, 2)) - 7;
            int timeToIntMinus7 = Integer.valueOf(timeTo.substring(0, 2)) - 7;
            if (timeToIntMinus7 == 12) timeToIntMinus7--; //in case if the last hour is 19

            for (int i = 0; i <= timeRange; i++) {
                if (i > 0 && i < timeRange)
                    holder.progressBars[timeFromIntMinus7 + i].setProgress(100);
                else if (i == 0) {
                    holder.progressBars[timeFromIntMinus7].setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                    int diff = 100 - getProgressVal(timeFrom, true);
                    holder.progressBars[timeFromIntMinus7].setProgress(diff);
                } else if (i == timeRange) {
                    if (!timeTo.substring(3).equals("00"))
                        holder.progressBars[timeToIntMinus7].setProgress(getProgressVal(timeTo, false));
                }
            }
        }
    }

    private int getProgressVal(String time, boolean isTimeFrom) {
        int res = 0;

        if (time.substring(3, 5).equals("00")) {
            res += isTimeFrom ? 0 : 100;
        } else if (time.substring(3, 5).equals("15"))
            res += 25;
        else if (time.substring(3, 5).equals("30"))
            res += 50;
        else if (time.substring(3, 5).equals("45"))
            res += 75;

        return res;
    }

    @Override
    public int getItemCount() {
        return mRoomsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name, tv_location, tv_size_capacity, tv_equipment;
        private ProgressBar[] progressBars = new ProgressBar[12];

        ViewHolder(View view) {
            super(view);

            //region Values of the adapter
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            tv_location = (TextView) view.findViewById(R.id.tv_location);
            tv_size_capacity = (TextView) view.findViewById(R.id.tv_size_capacity);
            tv_equipment = (TextView) view.findViewById(R.id.tv_equipment);

            progressBars[0] = (ProgressBar) view.findViewById(R.id.progressBar_7);
            progressBars[1] = (ProgressBar) view.findViewById(R.id.progressBar_8);
            progressBars[2] = (ProgressBar) view.findViewById(R.id.progressBar_9);
            progressBars[3] = (ProgressBar) view.findViewById(R.id.progressBar_10);
            progressBars[4] = (ProgressBar) view.findViewById(R.id.progressBar_11);
            progressBars[5] = (ProgressBar) view.findViewById(R.id.progressBar_12);
            progressBars[6] = (ProgressBar) view.findViewById(R.id.progressBar_13);
            progressBars[7] = (ProgressBar) view.findViewById(R.id.progressBar_14);
            progressBars[8] = (ProgressBar) view.findViewById(R.id.progressBar_15);
            progressBars[9] = (ProgressBar) view.findViewById(R.id.progressBar_16);
            progressBars[10] = (ProgressBar) view.findViewById(R.id.progressBar_17);
            progressBars[11] = (ProgressBar) view.findViewById(R.id.progressBar_18);
            //endregion
        }
    }

    void filterWithRoomName(String text) {
        mRoomsList.clear();
        if (text.isEmpty()) {
            mRoomsList.addAll(itemsCopy);
        } else {
            text = text.toLowerCase();
            for (Room item : itemsCopy) {
                if (item.getName().toLowerCase().contains(text)) {
                    mRoomsList.add(item);
                }
            }
        }
        notifyDataSetChanged();
    }

    void filterNextHour(boolean toFilter) {
        mRoomsList.clear();
        if (toFilter) {
            String nextHour = String.valueOf(CalendarTreatment.getCalendarParts(4)+1);
            nextHour = nextHour.length() == 1 ? "0" + nextHour + ":" : nextHour + ":";
            for (Room item : itemsCopy) {
                for (String time : item.getAvail()) {
                    if (time.contains(nextHour)) {
                        mRoomsList.add(item);
                    }
                }
            }
        } else {
            mRoomsList.addAll(itemsCopy);
        }
        notifyDataSetChanged();
    }
}
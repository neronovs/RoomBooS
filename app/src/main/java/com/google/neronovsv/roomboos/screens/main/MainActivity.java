package com.google.neronovsv.roomboos.screens.main;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.neronovsv.roomboos.R;
import com.google.neronovsv.roomboos.model.Room;
import com.google.neronovsv.roomboos.network.NetworkUtils;
import com.google.neronovsv.roomboos.network.usecase.RoomsUseCase;
import com.google.neronovsv.roomboos.repository.RepositoryProvider;
import com.google.neronovsv.roomboos.screens.details.DetailActivity;
import com.google.neronovsv.roomboos.screens.general.LoadingDialog;
import com.google.neronovsv.roomboos.screens.general.LoadingView;
import com.google.neronovsv.roomboos.utils.CalendarTreatment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

public class MainActivity
        extends AppCompatActivity
        implements RoomsView,
        DataAdapter.OnItemClickListener {

    //region Fields declaration
    private final String TAG = getClass().getName();
    private final String SEARCH_KEY_NOW = "now";
    private final String SEARCH_KEY_TODAY = "today";
    int DIALOG_DATE = 1;

    @BindView(R.id.tool_bar_widget)
    Toolbar toolbar;

    @BindView(R.id.recyclerView)
    RecyclerView roomsRecycler;

    @BindView(R.id.empty)
    View mEmptyView;

    @BindView(R.id.main_fl_container)
    FrameLayout main_fl_container;

    @BindView(R.id.toolbar_bottom_search_view)
    SearchView toolbar_bottom_search_view;

    @BindView(R.id.toolbar_bottom_hour_checkbox)
    CheckBox toolbar_bottom_hour_checkbox;

    private DataAdapter dataAdapter;
    private LoadingView loadingView;
    private CompositeDisposable mCompositeDisposable;
    private RoomsPresenter presenter;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        setListenersForFilters();

//        int columns = 1;
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        roomsRecycler.setLayoutManager(layoutManager);
        roomsRecycler.setHasFixedSize(true);
        //Init dataAdapter and set it to the roomRecycler after roomList getting from a request

        loadingView = LoadingDialog.view(getSupportFragmentManager());

        RoomsUseCase roomsUseCase = new RoomsUseCase(RepositoryProvider.getRoomsRepository());
        presenter = new RoomsPresenter(this, roomsUseCase);

        if (NetworkUtils.isOnline(this))
            presenter.init(SEARCH_KEY_TODAY);
    }

    private void setListenersForFilters() {
        toolbar_bottom_search_view.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                dataAdapter.filterWithRoomName(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                dataAdapter.filterWithRoomName(newText);
                return true;
            }
        });

        toolbar_bottom_hour_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataAdapter.filterNextHour(toolbar_bottom_hour_checkbox.isChecked());
            }
        });
    }

    //region Preparing the menu (action bar)
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Log.d(TAG, "onPrepareOptionsMenu() was started in MainActivity");
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu() was started in MainActivity");
        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected() was started in MainActivity");

        if (NetworkUtils.isOnline(this)) {
            switch (item.getItemId()) {
                case R.id.main_menuitem_show_now:
                    presenter.init(SEARCH_KEY_NOW);
                    break;
                case R.id.main_menuitem_choose_date:
                    new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            presenter.init(String.valueOf(CalendarTreatment.componentTimeToTimestamp(
                                    year, monthOfYear, dayOfMonth)));
                        }
                    }, CalendarTreatment.getCalendarParts(1),
                            CalendarTreatment.getCalendarParts(2),
                            CalendarTreatment.getCalendarParts(3)).show();
                    break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion

    @Override
    public void showError(Throwable throwable) {
        Log.i(TAG, " showError() Error is: " + throwable);
        roomsRecycler.setVisibility(View.GONE);
        mEmptyView.setVisibility(View.VISIBLE);
        Toast.makeText(this, "Error: " + throwable.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showRooms(@NonNull List<Room> roomList) {
        Log.i(TAG, " showRooms(). roomList is:  " + roomList);

        if (dataAdapter == null)
            dataAdapter = new DataAdapter((ArrayList<Room>) roomList, this);

        if (roomsRecycler.getAdapter() == null) {
            //Set animation for recyclerView
            ScaleInAnimationAdapter animationAdapter = new ScaleInAnimationAdapter(dataAdapter);
            animationAdapter.setDuration(400);
            roomsRecycler.setAdapter(animationAdapter);
        } else {
            dataAdapter.getmRoomsList().clear();
            dataAdapter.getmRoomsList().addAll(roomList);
            dataAdapter.notifyDataSetChanged();
        }

        roomsRecycler.setVisibility(View.VISIBLE);
        mEmptyView.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mCompositeDisposable != null)
            mCompositeDisposable.clear();
    }

    @Override
    public void onItemClick(@NonNull View view, @NonNull Room room) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("name", room.getName());
        startActivity(intent);
    }

    @Override
    public void showLoadingIndicator() {
        loadingView.showLoadingIndicator();
    }

    @Override
    public void hideLoadingIndicator() {
        loadingView.hideLoadingIndicator();
    }

}

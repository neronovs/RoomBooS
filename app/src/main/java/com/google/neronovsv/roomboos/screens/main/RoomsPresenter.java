package com.google.neronovsv.roomboos.screens.main;

import android.support.annotation.NonNull;

import com.google.neronovsv.roomboos.network.usecase.RoomsUseCase;

import java.util.Calendar;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

class RoomsPresenter {

    private final String TAG = getClass().getName();
    private final RoomsView roomsView;
    private final RoomsUseCase roomsUseCase;

    RoomsPresenter(@NonNull RoomsView roomsView, @NonNull RoomsUseCase roomsUseCase) {
        this.roomsView = roomsView;
        this.roomsUseCase = roomsUseCase;
    }

    void init(String searchKey) {
        roomsUseCase.getRoomsObservable(searchKey)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(roomsView::showLoadingIndicator)
                .doAfterTerminate(roomsView::hideLoadingIndicator)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(roomsView::showRooms, roomsView::showError);
    }
}
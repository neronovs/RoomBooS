package com.google.neronovsv.roomboos.screens.general;

public interface LoadingView {

    void showLoadingIndicator();

    void hideLoadingIndicator();

}

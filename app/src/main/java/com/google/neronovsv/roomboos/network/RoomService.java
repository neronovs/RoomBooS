package com.google.neronovsv.roomboos.network;

import com.google.neronovsv.roomboos.model.Room;

import java.util.List;
import java.util.Map;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

public interface RoomService {

    @POST("getrooms/")
    Observable<List<Room>> roomsService(
            @Body() Map token
    );

}

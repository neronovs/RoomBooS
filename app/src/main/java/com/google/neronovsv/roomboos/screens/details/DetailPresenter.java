package com.google.neronovsv.roomboos.screens.details;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.widget.ProgressBar;

import com.google.neronovsv.roomboos.cash.RoomsCacheTransformer;
import com.google.neronovsv.roomboos.model.Room;

import java.util.List;

class DetailPresenter {

    private final String TAG = getClass().getName();
    private final DetailView detailView;
    private Room room = null;

    DetailPresenter(@NonNull DetailView detailView, String roomName, ProgressBar[] progressBars) {
        this.detailView = detailView;

        //Getting a specific room from Realm keeping
        room = RoomsCacheTransformer.getSpecificRoomFromRealm(roomName);

        if (room != null) {
            StringBuilder equipmet = new StringBuilder();
            for (String el : room.getEquipment()) {
                equipmet.append(el).append("; ");
            }
            detailView.fillWidgets(room.getLocation(),
                room.getSize(),
                room.getName(),
                equipmet.toString());
        }

        fillTimeLine(progressBars);
    }

    private void fillTimeLine(ProgressBar[] progressBars) {
        List<String> listAvail = room.getAvail();

        for (int j = 0; j < listAvail.size(); j++) {
            String timeFrom = listAvail.get(j).substring(0, 6);
            String timeTo = listAvail.get(j).substring(8);
            int timeRange = Integer.valueOf(timeTo.substring(0, 2)) -
                    Integer.valueOf(timeFrom.substring(0, 2));
            int timeFromIntMinus7 = Integer.valueOf(timeFrom.substring(0, 2)) - 7;
            int timeToIntMinus7 = Integer.valueOf(timeTo.substring(0, 2)) - 7;
            if (timeToIntMinus7 == 12) timeToIntMinus7--; //in case if the last hour is 19

            for (int i = 0; i <= timeRange; i++) {
                if (i > 0 && i < timeRange)
                    progressBars[timeFromIntMinus7 + i].setProgress(100);
                else if (i == 0) {
                    progressBars[timeFromIntMinus7].setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                    int diff = 100 - getProgressVal(timeFrom, true);
                    progressBars[timeFromIntMinus7].setProgress(diff);
                } else if (i == timeRange) {
                    if (!timeTo.substring(3).equals("00"))
                        progressBars[timeToIntMinus7].setProgress(getProgressVal(timeTo, false));
                }
            }
        }
    }

    private int getProgressVal(String time, boolean isTimeFrom) {
        int res = 0;

        if (time.substring(3, 5).equals("00")) {
            res += isTimeFrom ? 0 : 100;
        } else if (time.substring(3, 5).equals("15"))
            res += 25;
        else if (time.substring(3, 5).equals("30"))
            res += 50;
        else if (time.substring(3, 5).equals("45"))
            res += 75;

        return res;
    }

    PagerAdapter pagerAdapterCreater(Context context) {
        return new MyFragmentPagerAdapter(context, room);
    }
}
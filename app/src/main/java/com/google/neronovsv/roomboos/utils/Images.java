package com.google.neronovsv.roomboos.utils;

import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.google.neronovsv.roomboos.BuildConfig;
import com.google.neronovsv.roomboos.R;
import com.google.neronovsv.roomboos.RoomsApp;
import com.google.neronovsv.roomboos.model.Room;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public final class Images {

    private static final String API_ENDPOINT = BuildConfig.API_ENDPOINT;

    private Images() {
    }

    public static void loadImageList(@NonNull ArrayList<ImageView> imageViews, @NonNull Room room) {
        for (int i = 0; i < room.getImages().size(); i++) {
            imageViews.add(new ImageView(RoomsApp.getAppContext()));
            loadImage(imageViews.get(i), room.getImages().get(i));
        }
    }

    public static void loadImage(@NonNull ImageView imageView, @NonNull String posterPath) {
        String url = API_ENDPOINT + posterPath;
        Picasso.with(imageView.getContext())
                .load(url)
                .error(R.layout.empty)
                .noFade()
                .into(imageView);
    }

    public static void fetch(@NonNull String posterPath, @NonNull String size) {
        String url = BuildConfig.API_ENDPOINT + posterPath;
        Picasso.with(RoomsApp.getAppContext())
                .load(url)
                .fetch();
    }
}

package com.google.neronovsv.roomboos.screens.details;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.neronovsv.roomboos.R;
import com.google.neronovsv.roomboos.model.Room;
import com.google.neronovsv.roomboos.utils.Images;

class MyFragmentPagerAdapter extends PagerAdapter {

    private LayoutInflater mLayoutInflater;
    private Room room;
    private int count;

    MyFragmentPagerAdapter(Context context, Room room) {
        this.room = room;
        this.count = room.getImages().size();
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.image_pager, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.image_pager_view);
        Images.loadImage(imageView, room.getImages().get(position));


        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
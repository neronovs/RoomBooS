package com.google.neronovsv.roomboos.screens.main;

import android.support.annotation.NonNull;

import com.google.neronovsv.roomboos.model.Room;

import java.util.List;

public interface RoomsView extends com.google.neronovsv.roomboos.screens.general.LoadingView {

    void showRooms(@NonNull List<Room> rooms);

    void showError(Throwable throwable);

}



package com.google.neronovsv.roomboos.network.usecase;

import com.google.neronovsv.roomboos.model.Room;

import java.util.List;

import rx.Observable;

public class RoomsUseCase {

    private final String TAG = getClass().getName();
    private final RoomsRepository mRepository;

    public RoomsUseCase(RoomsRepository repository) {
        mRepository = repository;
    }

    public Observable<List<Room>> getRoomsObservable(String searchKey) {
        return mRepository.getRoomsObservable(searchKey);
    }
}



package com.google.neronovsv.roomboos.utils;

import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.google.neronovsv.roomboos.BuildConfig;
import com.google.neronovsv.roomboos.R;
import com.google.neronovsv.roomboos.RoomsApp;
import com.google.neronovsv.roomboos.model.Room;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public final class CalendarTreatment {

    public static int getCalendarParts(int calType) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("EEEE, MMMM d, yyyy 'at' HH:mm a");
        format.format(c.getTime());

        int res = 0;
        switch (calType) {
            case 1:
                res = c.get(Calendar.YEAR);
                break;
            case 2:
                res =  c.get(Calendar.MONTH);
                break;
            case 3:
                res =  c.get(Calendar.DAY_OF_MONTH);
                break;
            case 4:
                res =  c.get(Calendar.HOUR_OF_DAY);
                break;
            case 5:
                res =  c.get(Calendar.MINUTE);
                break;
            case 6:
                res =  c.get(Calendar.SECOND);
                break;
            case 7:
                res =  c.get(Calendar.MILLISECOND);
                break;
        }

        return res;
    }

    public static int componentTimeToTimestamp(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);

        return (int) (c.getTimeInMillis() / 1000L);
    }
}

package com.google.neronovsv.roomboos.cash;

import com.google.neronovsv.roomboos.model.Room;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Observable;
import rx.functions.Func1;

public class RoomsCacheTransformer implements Observable.Transformer<List<Room>, List<Room>> {

    private final Func1<List<Room>, Observable<List<Room>>> mSaveFunc = rooms -> {
        Realm.getDefaultInstance().executeTransaction(realm -> {
            realm.delete(Room.class);
            realm.insert(rooms);
        });
        return Observable.just(rooms);
    };

    private final Func1<Throwable, Observable<List<Room>>> mCacheErrorHandler = throwable -> {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Room> results = realm.where(Room.class).findAll();
        return Observable.just(realm.copyFromRealm(results));
    };

    @Override
    public Observable<List<Room>> call(Observable<List<Room>> roomsObservable) {
        return roomsObservable
                .flatMap(mSaveFunc)
                .onErrorResumeNext(mCacheErrorHandler);
    }

    public static Room getSpecificRoomFromRealm(String roomName) {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Room.class)
                .equalTo("name", roomName)
                .findFirst();
    }
}

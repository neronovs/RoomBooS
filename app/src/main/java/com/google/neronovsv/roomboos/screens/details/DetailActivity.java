package com.google.neronovsv.roomboos.screens.details;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.neronovsv.roomboos.R;
import com.google.neronovsv.roomboos.cash.RoomsCacheTransformer;
import com.google.neronovsv.roomboos.model.Room;
import com.google.neronovsv.roomboos.screens.general.LoadingView;
import com.google.neronovsv.roomboos.screens.main.RoomsView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity
        extends AppCompatActivity
        implements DetailView {

    //region Fields declaration
    private final String TAG = getClass().getName();
    private LoadingView loadingView;

    public PagerAdapter getPagerAdapter() {
        return pagerAdapter;
    }
    private PagerAdapter pagerAdapter;

    private ProgressBar[] progressBars = new ProgressBar[12];
    private DetailPresenter detailPresenter;

    @BindView(R.id.tool_bar_widget)
    Toolbar toolbar;

    @BindView(R.id.detailed_image_pager)
    ViewPager detailed_image_pager;

    @BindView(R.id.detailed_tv_location)
    TextView detailed_tv_location;

    @BindView(R.id.detailed_tv_size_capacity)
    TextView detailed_tv_size_capacity;

    @BindView(R.id.detailed_tv_name)
    TextView detailed_tv_name;

    @BindView(R.id.detailed_tv_equipment)
    TextView detailed_tv_equipment;

    //endregion

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailed_view);
        ButterKnife.bind(this);

        bindTimeLine();

        //region ToolBar treatment
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.mipmap.back_arrow_blue_mini));

        setSupportActionBar(toolbar);
//        if (getSupportActionBar() != null)
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        //endregion

        Intent intent = getIntent();
        String roomName = intent.getStringExtra("name");
        detailPresenter = new DetailPresenter(this, roomName, progressBars);

        bindTimeLine();

        pagerAdapter = detailPresenter.pagerAdapterCreater(this);
        detailed_image_pager.setAdapter(pagerAdapter);
    }

    private void bindTimeLine() {
        progressBars[0] = (ProgressBar) findViewById(R.id.progressBar_7);
        progressBars[1] = (ProgressBar) findViewById(R.id.progressBar_8);
        progressBars[2] = (ProgressBar) findViewById(R.id.progressBar_9);
        progressBars[3] = (ProgressBar) findViewById(R.id.progressBar_10);
        progressBars[4] = (ProgressBar) findViewById(R.id.progressBar_11);
        progressBars[5] = (ProgressBar) findViewById(R.id.progressBar_12);
        progressBars[6] = (ProgressBar) findViewById(R.id.progressBar_13);
        progressBars[7] = (ProgressBar) findViewById(R.id.progressBar_14);
        progressBars[8] = (ProgressBar) findViewById(R.id.progressBar_15);
        progressBars[9] = (ProgressBar) findViewById(R.id.progressBar_16);
        progressBars[10] = (ProgressBar) findViewById(R.id.progressBar_17);
        progressBars[11] = (ProgressBar) findViewById(R.id.progressBar_18);
    }

    @Override
    public void showLoadingIndicator() {
        loadingView.showLoadingIndicator();
    }

    @Override
    public void hideLoadingIndicator() {
        loadingView.hideLoadingIndicator();
    }

    @Override
    public void fillWidgets(String detailed_tv_location_txt, String detailed_tv_size_capacity_txt,
                            String detailed_tv_name_txt, String detailed_tv_equipment_txt) {
        this.detailed_tv_location.setText(detailed_tv_location_txt);
        this.detailed_tv_size_capacity.setText(detailed_tv_size_capacity_txt);
        this.detailed_tv_name.setText(detailed_tv_name_txt);
        this.detailed_tv_equipment.setText(detailed_tv_equipment_txt);
    }
}

package com.google.neronovsv.roomboos.repository;

import com.google.neronovsv.roomboos.cash.RoomsCacheTransformer;
import com.google.neronovsv.roomboos.model.Room;
import com.google.neronovsv.roomboos.network.ApiFactory;
import com.google.neronovsv.roomboos.network.usecase.RoomsRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.schedulers.Schedulers;

public class RoomsDataRepository
        implements RoomsRepository {

    @Override
    public Observable<List<Room>> getRoomsObservable(String searchKey) {
        Map<String, String> mapToBody = new HashMap<>();
        mapToBody.put("date", searchKey);

        return ApiFactory.getRoomsService()
                .roomsService(mapToBody)
                .subscribeOn(Schedulers.io())
                .compose(new RoomsCacheTransformer())
                .flatMap(Observable::from)
                .toList();
    }
}
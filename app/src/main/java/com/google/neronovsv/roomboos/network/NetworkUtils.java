package com.google.neronovsv.roomboos.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class NetworkUtils {
    //The method checks the Internet connection
    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        boolean isInternet = netInfo != null && netInfo.isConnectedOrConnecting();

        if (!isInternet)
            Toast.makeText(context, "The Internet connection is lost", Toast.LENGTH_SHORT).show();

        return isInternet;
    }
}

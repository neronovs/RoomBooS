package com.google.neronovsv.roomboos.network;

import com.google.neronovsv.roomboos.BuildConfig;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ApiFactory {

    private static final String API_ENDPOINT_URL = BuildConfig.API_ENDPOINT; //"https://challenges.1aim.com/roombooking_app/";
    private static OkHttpClient sClient;

    private static RoomService sService;

    public static RoomService getRoomsService() {
        //I know that double checked locking is not a good pattern, but it's enough here
        RoomService service = sService;
        if (service == null) {
            synchronized (ApiFactory.class) {
                service = sService;
                if (service == null) {
                    service = sService = createService();
                }
            }
        }
        return service;
    }

    private static RoomService createService() {
        return new Retrofit.Builder()
                .baseUrl(API_ENDPOINT_URL)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
                .create(RoomService.class);
    }

    private static OkHttpClient getClient() {
        OkHttpClient client = sClient;
        if (client == null) {
            synchronized (ApiFactory.class) {
                client = sClient;
                if (client == null) {
                    client = sClient = buildClient();
                }
            }
        }
        return client;
    }

    private static OkHttpClient buildClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new ApiKeyInterceptor())
                .build();
    }
}

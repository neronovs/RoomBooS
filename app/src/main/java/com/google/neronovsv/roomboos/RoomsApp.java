package com.google.neronovsv.roomboos;

import android.app.Application;
import android.support.annotation.NonNull;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.rx.RealmObservableFactory;

public class RoomsApp extends Application {

    private static RoomsApp sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

//        roomArrayList = new ArrayList<>();

        Picasso picasso = new Picasso.Builder(this)
                .downloader(new OkHttp3Downloader(this))
                .build();
        Picasso.setSingletonInstance(picasso);

        Realm.init(this);

        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .rxFactory(new RealmObservableFactory())
                .build();
        Realm.setDefaultConfiguration(configuration);
    }

    @NonNull
    public static RoomsApp getAppContext() {
        return sInstance;
    }
}

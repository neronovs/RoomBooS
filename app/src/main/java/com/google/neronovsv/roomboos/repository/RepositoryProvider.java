package com.google.neronovsv.roomboos.repository;

import com.google.neronovsv.roomboos.network.usecase.RoomsRepository;

public class RepositoryProvider {

    private static RoomsRepository sRoomsRepository;

    public static RoomsRepository getRoomsRepository() {
        if (sRoomsRepository == null) {
            sRoomsRepository = new RoomsDataRepository();
        }
        return sRoomsRepository;
    }

    public static void setRoomsRepository(RoomsRepository roomsRepository) {
        sRoomsRepository = roomsRepository;
    }

}
